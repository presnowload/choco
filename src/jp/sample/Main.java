package jp.sample;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

public class Main {
    private static final boolean DEBUG = false;
    private static final String TEXT = DEBUG ? "sample.txt" : "problem.txt";

    /**
     * @param args
     */
    public static void main(String[] args) {

        //        for (int i = 0; i < 100000; i++) {
        //            boolean r = MRPrimalityTest.isPrimeNumber(i, 10);
        //            if (!r)
        //                continue;
        //            System.out.println(i + ":" + r);
        //        }
        //        if (true)
        //            return;

        long startTime = System.currentTimeMillis();
        double N = 0;

        BufferedReader b = null;
        try {
            FileReader fr = new FileReader(TEXT);
            b = new BufferedReader(fr);
            String s;

            while ((s = b.readLine()) != null) {
                if (s.isEmpty())
                    continue;
                N = Double.parseDouble(s.trim().replace("N=", ""));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                b.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Is N prime number?
        int n = 99551;
        int k = 10;
        System.out.println(MRPrimalityTest.isPrimeNumber(n, k));
        // 素数で無ければ約数を見つける
        // 約数を取得

        print(N, startTime);

    }

    private static void print(double n, long startTime) {
        print(new BigDecimal(n), startTime);
    }

    private static void print(BigDecimal n, long startTime) {
        System.out.println(n + "," + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime));
        System.out.println("ENV: Java");
    }

}
