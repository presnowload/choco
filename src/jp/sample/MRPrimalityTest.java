package jp.sample;

import java.math.BigDecimal;

/**
 * Primality Test Class.
 *
 */
public class MRPrimalityTest {
    /**
     *  n > 1 : 素数判定対象の奇数の整数; k : 判定の正確度を指定するパラメータ.
     * @param n
     * @param k
     * @return true if this number is probably prime number, false otherwise(probably composite number).
     */
    public static boolean isPrimeNumber(double n, int k) {
        // 2 is prime number.
        if (n == 2)
            return true;
        // (1 or even) is not prime number.
        if (n == 1 || isEven(n))
            return false;
        // n - 1 を 2 のべき乗で割って、 2^s * d の形式にする。
        double d = n - 1;
        //        int s = 0;
        while (isEven(d)) {
            //d >>= 1;
            d = Math.floor(d / 2);
            //            s++;
        }
        for (int i = 0; i < k; i++) {

            // [1, n - 1] の範囲から a をランダムに選ぶ。
            double a = (double)(Math.round((n - 2) * Math.random()) + 1);
            double t = d;
            double y = pow(a, t, n);

            while (t != n - 1 && y != 1 && y != n - 1) {
                //y = (y * y) % n;
                y = mod(y * y, n);
                //t <<= 1;
                t = t * 2;
            }
            if (y != n - 1 && isEven(t))
                return false;

        }
        return true;
    }

    /**
     * べき乗演算.
     * @param base
     * @param power
     * @param mod
     * @return
     */
    private static double pow(double base, double power, double mod) {
        double result = 1;
        while (power > 0) {
            if (power % 2 == 1)
                result = (result * base) % mod;
            base = (base * base) % mod;
            //power >>= 1; 
            power = Math.floor(power / 2);
        }
        return result;
    }

    private static boolean isEven(double n) {
        return isEven(new BigDecimal(n));
    }

    private static boolean isEven(BigDecimal n) {
        return mod(n, 2).compareTo(new BigDecimal(0)) == 0;
    }

    private static double mod(double d, double n) {
        return mod(new BigDecimal(d), new BigDecimal(n)).doubleValue();
    }

    private static BigDecimal mod(BigDecimal m, int n) {
        return mod(m, new BigDecimal(n));
    }

    private static BigDecimal mod(BigDecimal m, BigDecimal n) {
        BigDecimal[] results = m.divideAndRemainder(n);
        return results[1];
    }

}
